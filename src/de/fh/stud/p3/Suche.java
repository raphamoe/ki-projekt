package de.fh.stud.p3;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import de.fh.stud.p2.Knoten;

public class Suche {

	private Knoten start;

	public Suche(Knoten start) {
		this.start = start;
	}


	public Knoten startDfs() {
		Long startTimer = System.currentTimeMillis();
		Deque<Knoten> stack = new LinkedList<>();
		List<Knoten> closedList = new LinkedList<>();
		stack.push(start);

		while (!stack.isEmpty()) {
			Knoten knoten = stack.pop();
			if (knoten.isEnd()) {
				System.out.println(System.currentTimeMillis() - startTimer + "ms");
				System.out.println("Länge der Closed List: " +closedList.size());
				System.out.println("Länge der Open List: " +stack.size());
				return knoten;
			}
			if (!closedList.contains(knoten)) {
				closedList.add(knoten);
				knoten.expand().forEach(stack::push);
			}
		}

		return null;
	}

	public Knoten startBfs() {
		Long startTimer = System.currentTimeMillis();
		Queue<Knoten> queue = new LinkedList<>();
		List<Knoten> closedList = new LinkedList<>();
		queue.add(start);

		while (!queue.isEmpty()) {
			Knoten knoten = queue.poll();
			if (knoten.isEnd()) {
				System.out.println(System.currentTimeMillis() - startTimer + "ms");
				System.out.println("Länge der Closed List: " +closedList.size());
				System.out.println("Länge der Open List: " +queue.size());
				return knoten;
			}
			if (!closedList.contains(knoten)) {
				closedList.add(knoten);
				knoten.expand().forEach(queue::add);
			}
		}

		return null;
	}

	public Knoten startUCS() {
		Long startTimer = System.currentTimeMillis();

		PriorityQueue<Knoten> openList = new PriorityQueue<>((a, b) -> a.getDistance() - b.getDistance());
		List<Knoten> closedList = new LinkedList<>();

		openList.add(start);

		while (!openList.isEmpty()) {
			Knoten knoten = openList.poll();
			if (knoten.isEnd()) {
				System.out.println(System.currentTimeMillis() - startTimer + "ms");
				System.out.println("Länge der Closed List: " +closedList.size());
				System.out.println("Länge der Open List: " +openList.size());
				return knoten;
			}
			if (!closedList.contains(knoten)) {
				closedList.add(knoten);
				knoten.expand().forEach(openList::add);
			}
		}

		return null;
	}

	public Knoten startGreedy() {
		Long startTimer = System.currentTimeMillis();

		PriorityQueue<Knoten> openList = new PriorityQueue<>((a, b) -> a.getHeuristic() - b.getHeuristic());
		List<Knoten> closedList = new LinkedList<>();

		openList.add(start);

		openList.add(start);
		while (!openList.isEmpty()) {
			Knoten knoten = openList.poll();
			if (knoten.isEnd()) {
				System.out.println(System.currentTimeMillis() - startTimer + "ms");
				System.out.println("Länge der Closed List: " +closedList.size());
				System.out.println("Länge der Open List: " +openList.size());
				return knoten;
			}
			if (!closedList.contains(knoten)) {
				closedList.add(knoten);
				knoten.expand().forEach(openList::add);
			}
		}

		return null;
	}

	public Knoten startAstar() {
		Long startTimer = System.currentTimeMillis();
		List<Knoten> neighbours = new LinkedList<>();

		PriorityQueue<Knoten> openList = new PriorityQueue<>(
				(a, b) -> (a.getDistance() + a.getHeuristic()) - (b.getDistance() + b.getHeuristic()));
		List<Knoten> closedList = new ArrayList<>();

		openList.add(start);
		while (!openList.isEmpty()) {
			Knoten knoten = openList.poll();
			if (knoten.isEnd()) {
				System.out.println(System.currentTimeMillis() - startTimer + "ms");
				System.out.println("Länge der Closed List: " +closedList.size());
				System.out.println("Länge der Open List: " +openList.size());
				return knoten;
			}
			if (!closedList.contains(knoten)) {
				closedList.add(knoten);
				neighbours = knoten.expand();
				for (Knoten k : neighbours) {
					if(!closedList.contains(k) && !openList.contains(k)) {
						openList.add(k);
					}
//					if (closedList.contains(k)) {
//						continue;
//					} else {
//						int newTotalCost = knoten.getDistance() + knoten.getHeuristic();
//						if (newTotalCost < k.getDistance() || !openList.contains(k)) {
//							openList.add(k);
//						}
//
//					}
				}
			}
		}

		return null;
	}
}
