package de.fh.stud.p3;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import de.fh.kiServer.agents.Agent;
import de.fh.pacman.PacmanAgent_2021;
import de.fh.pacman.PacmanGameResult;
import de.fh.pacman.PacmanPercept;
import de.fh.pacman.PacmanStartInfo;
import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanActionEffect;
import de.fh.stud.p2.Knoten;

public class ITAAgent_P3 extends PacmanAgent_2021 {

	/**
	 * Die als nächstes auszuführende Aktion
	 */
	private PacmanAction nextAction;

	/**
	 * Der gefundene Lösungknoten der Suche
	 */
	private Knoten loesungsKnoten;
	private int choice;

	private List<PacmanAction> actionList;

	public ITAAgent_P3(String name) {
		super(name);
		choice = chooseAlgo();
		if (choice <= -1) {
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		ITAAgent_P3 agent = new ITAAgent_P3("ITAAgent_P3");
		Agent.start(agent, "127.0.0.1", 5000);
	}

	/**
	 * @param percept      - Aktuelle Wahrnehmung des Agenten, bspw. Position der
	 *                     Geister und Zustand aller Felder der Welt.
	 * @param actionEffect - Aktuelle Rückmeldung des Server auf die letzte
	 *                     übermittelte Aktion.
	 */
	@Override
	public PacmanAction action(PacmanPercept percept, PacmanActionEffect actionEffect) {

		// Util.printView(percept.getView());
		

		if (loesungsKnoten == null) {
			Suche suche = new Suche(new Knoten(percept.getView(), percept.getPosX(), percept.getPosY()));

			switch (choice) {
			case 0:
				loesungsKnoten = suche.startDfs();
				break;
			case 1:
				loesungsKnoten = suche.startBfs();
				break;
			case 2:
				loesungsKnoten = suche.startUCS();
				break;
			case 3:
				loesungsKnoten = suche.startGreedy();
				break;
			case 4:
				loesungsKnoten = suche.startAstar();
				break;
			}

			actionList = loesungsKnoten.getActionList();
			System.out.println("Länge der Lösung: "  + actionList.size());
		}

		if (loesungsKnoten != null) {
			nextAction = actionList.isEmpty() ? PacmanAction.QUIT_GAME : actionList.remove(0);

		} else {
			nextAction = PacmanAction.QUIT_GAME;
		}

		return nextAction;
	}
	
	private static int chooseAlgo() {
		JFrame jf = new JFrame();
		jf.setAlwaysOnTop(true);
		String[] values = { "DFS", "BFS", "UCS", "Greedy", "A*" };
		Object selected = JOptionPane.showInputDialog(jf, "Welcher Algorithmus soll verwendet werden?", "Selection",
				JOptionPane.DEFAULT_OPTION, null, values, "0");
		if (selected != null) {
			String selectedString = selected.toString();
			switch (selectedString) {
			case "DFS":
				return 0;
			case "BFS":
				return 1;
			case "UCS":
				return 2;
			case "Greedy":
				return 3;
			case "A*":
				return 4;
			}
		} else {
			return -1;
		}
		return -1;
	}

	@Override
	protected void onGameStart(PacmanStartInfo startInfo) {

	}

	@Override
	protected void onGameover(PacmanGameResult gameResult) {

	}

}
