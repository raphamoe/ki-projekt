package de.fh.stud.p2;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import de.fh.kiServer.util.Util;
import de.fh.pacman.enums.PacmanTileType;

public class BaumTest {

	public static void main(String[] args) {
		// Anfangszustand nach Aufgabe
		PacmanTileType[][] view = {
				{ PacmanTileType.WALL, PacmanTileType.WALL, PacmanTileType.WALL, PacmanTileType.WALL },
				{ PacmanTileType.WALL, PacmanTileType.EMPTY, PacmanTileType.DOT, PacmanTileType.WALL },
				{ PacmanTileType.WALL, PacmanTileType.DOT, PacmanTileType.WALL, PacmanTileType.WALL },
				{ PacmanTileType.WALL, PacmanTileType.WALL, PacmanTileType.WALL, PacmanTileType.WALL } };
		// Startposition des Pacman
		int posX = 1, posY = 1;

		/*
		 * TODO Praktikum 2 [3]: Baut hier basierend auf dem gegebenen Anfangszustand
		 * (siehe view, posX und posY) den Suchbaum auf.
		 */

		Queue<Knoten> queue = new LinkedList<>();
		queue.add(new Knoten(view, posX, posY));

		for (int i = 1; i <= 10; i++) {
			try {
				Knoten knoten = queue.remove();
				knoten.expand().forEach(queue::add);

				System.out.printf("X: %s\nY: %s\n", knoten.getxPos(), knoten.getyPos());
				Util.printView(knoten.getView());
			} catch (NoSuchElementException e) {
				System.out.println("Keine weiter Knoten vorhanden");
			}
		}
	}
}
