package de.fh.stud.p2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanTileType;

public class Knoten {

	private PacmanTileType[][] view;
	private int xPos, yPos;
	private int distance, heuristic;
	private PacmanAction action;
	private Knoten parent;

	public Knoten(PacmanTileType[][] view, int xPos, int yPos) {
		this.view = view;
		this.xPos = xPos;
		this.yPos = yPos;
		distance = 0;
		parent = null;
		calcHeuristic();
	}

	public Knoten(Knoten knoten) {
		this.parent = knoten;
		this.xPos = knoten.xPos;
		this.yPos = knoten.yPos;
		distance = knoten.distance;
	}

	public PacmanTileType[][] getView() {
		return view;
	}

	public int getxPos() {
		return xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public Knoten getParent() {
		return parent;
	}

	public void setParent(Knoten k) {
		this.parent = k;
	}

	public PacmanAction getAction() {
		return action;
	}

	public int getDistance() {
		return distance;
	}

	public int getHeuristic() {
		return heuristic;
	}

	public Knoten goNorth() {
		int mov = move(view, xPos, yPos - 1);
		if (mov <= -1) {
			return null;
		}
		Knoten kind = new Knoten(this);
		kind.yPos--;
		if (mov == 2) {
			kind.view = view;
			kind.heuristic = heuristic;
		} else {
			kind.view = deepCopy(view);
			kind.view[kind.xPos][kind.yPos] = PacmanTileType.EMPTY;
			kind.calcHeuristic();
		}

		kind.action = PacmanAction.GO_NORTH;
		kind.distance++;
		return kind;
	}

	public Knoten goEast() {
		int mov = move(view, xPos + 1, yPos);
		if (mov <= -1) {
			return null;
		}
		Knoten kind = new Knoten(this);
		kind.xPos++;
		if (mov == 2) {
			kind.view = view;
			kind.heuristic = heuristic;
		} else {
			kind.view = deepCopy(view);
			kind.view[kind.xPos][kind.yPos] = PacmanTileType.EMPTY;
			kind.calcHeuristic();
		}
		kind.action = PacmanAction.GO_EAST;
		kind.distance++;
		return kind;
	}

	public Knoten goSouth() {
		int mov = move(view, xPos, yPos + 1);
		if (mov <= -1) {
			return null;
		}
		Knoten kind = new Knoten(this);
		kind.yPos++;
		if (mov == 2) {
			kind.view = view;
			kind.heuristic = heuristic;
		} else {
			kind.view = deepCopy(view);
			kind.view[kind.xPos][kind.yPos] = PacmanTileType.EMPTY;
			kind.calcHeuristic();
		}
		kind.action = PacmanAction.GO_SOUTH;
		kind.distance++;
		return kind;
	}

	public Knoten goWest() {
		int mov = move(view, xPos - 1, yPos);
		if (mov <= -1) {
			return null;
		}
		Knoten kind = new Knoten(this);
		kind.xPos--;
		if (mov == 2) {
			kind.view = view;
			kind.heuristic = heuristic;
		} else {
			kind.view = deepCopy(view);
			kind.view[kind.xPos][kind.yPos] = PacmanTileType.EMPTY;
			kind.calcHeuristic();
		}
		kind.action = PacmanAction.GO_WEST;
		kind.distance++;
		return kind;
	}

	private void calcHeuristic() {
		int dots = 0;
		for (PacmanTileType[] i : view) {
			for (PacmanTileType j : i) {
				if (j == PacmanTileType.DOT) {
					dots++;
				}
			}
		}
		heuristic = dots;
	}

	// Return:
	// -1 = false
	// 1=true
	// 2=true und dot sollte in empty geänder werden
	private static int move(PacmanTileType[][] view, int x, int y) {
		if (x < 0 || y < 0 || x > view.length || y > view[0].length || view[x][y] == PacmanTileType.WALL) {
			return -1;
		}
		if (view[x][y] == PacmanTileType.DOT) {
			return 1;
		}
		return 2;
	}

	private PacmanTileType[][] deepCopy(PacmanTileType[][] matrix) {
		return Arrays.stream(matrix).map(el -> el.clone()).toArray($ -> matrix.clone());
	}

	public boolean isEnd() {
		for (int i = 0; i < view.length; i++) {
			for (int j = 0; j < view[i].length; j++) {
				if (view[i][j] == PacmanTileType.DOT) {
					return false;
				}
			}
		}
		return true;
	}

	public List<PacmanAction> getActionList() {
		List<PacmanAction> actionList = new LinkedList<>();
		actionList.add(action);
		Knoten knoten = parent;
		while (knoten != null && knoten.action != null) {
			actionList.add(0, knoten.action);
			knoten = knoten.parent;
		}
		return actionList;
	}

	public List<Knoten> expand() {
		List<Knoten> childes = new ArrayList<>();
		Knoten knoten;
		if ((knoten = this.goNorth()) != null) {
			childes.add(knoten);

		}
		if ((knoten = this.goEast()) != null) {
			childes.add(knoten);

		}
		if ((knoten = this.goSouth()) != null) {
			childes.add(knoten);

		}
		if ((knoten = this.goWest()) != null) {
			childes.add(knoten);

		}

		// System.out.println(++expands);
		return childes;
	}

	@Override
	public boolean equals(Object o) {
//		if (!(o instanceof Knoten)) {
//			return false;
//		}
		Knoten prev = (Knoten) o;
		return Arrays.deepEquals(this.getView(), prev.getView()) && prev.getxPos() == xPos && prev.getyPos() == yPos;
	}
}
