package de.fh.stud.p1;

import de.fh.kiServer.agents.Agent;
import de.fh.kiServer.util.Util;
import de.fh.pacman.PacmanAgent_2021;
import de.fh.pacman.PacmanGameResult;
import de.fh.pacman.PacmanPercept;
import de.fh.pacman.PacmanStartInfo;
import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanActionEffect;

public class ITA_Agent_RM extends PacmanAgent_2021 {

	/*
	 * TODO Praktikum 1: Fügt gemäß der Aufgabenstellung neue Attribute hinzu, falls
	 * notwendig.
	 */
	
	private int countEmptyTiles = 0;

	/**
	 * Die als nächstes auszuführende Aktion
	 */
	private PacmanAction nextAction;

	public ITA_Agent_RM(String name) {
		super(name);
	}

	public static void main(String[] args) {
		ITA_Agent_RM agent = new ITA_Agent_RM("ITA_Agent_RM");
		Agent.start(agent, "127.0.0.1", 5000);
	}

	/**
	 * @param percept      - Aktuelle Wahrnehmung des Agenten, bspw. Position der
	 *                     Geister und Zustand aller Felder der Welt.
	 * @param actionEffect - Aktuelle Rückmeldung des Server auf die letzte
	 *                     übermittelte Aktion.
	 */
	@Override
	public PacmanAction action(PacmanPercept percept, PacmanActionEffect actionEffect) {
		
	

		// Gebe den aktuellen Zustand der Welt auf der Konsole aus
		Util.printView(percept.getView());

		// Nachdem das Spiel gestartet wurde, geht der Agent nach Osten
		if (actionEffect == PacmanActionEffect.GAME_INITIALIZED) {
			nextAction = PacmanAction.GO_EAST;
		}
		if (actionEffect == PacmanActionEffect.BUMPED_INTO_WALL) {
			if (nextAction == PacmanAction.GO_EAST) {
				nextAction = PacmanAction.GO_SOUTH;
			}
			else if (nextAction == PacmanAction.GO_SOUTH) {
				nextAction = PacmanAction.GO_WEST;
			}
			else if (nextAction == PacmanAction.GO_WEST) {
				nextAction = PacmanAction.GO_NORTH;
			}
			else if (nextAction == PacmanAction.GO_NORTH) {
				nextAction = PacmanAction.GO_EAST;
			}
		}
		
		if(actionEffect == PacmanActionEffect.MOVED_ON_EMPTY_TILE) {
			countEmptyTiles++;
			if(countEmptyTiles == 1) {
				if (nextAction == PacmanAction.GO_EAST) {
					nextAction = PacmanAction.GO_WEST;
				}
				else if (nextAction == PacmanAction.GO_SOUTH) {
					nextAction = PacmanAction.GO_NORTH;
				}
				else if (nextAction == PacmanAction.GO_WEST) {
					nextAction = PacmanAction.GO_EAST;
				}
				else if (nextAction == PacmanAction.GO_NORTH) {
					nextAction = PacmanAction.GO_SOUTH;
				}
			}
			if(countEmptyTiles == 2) {
				countEmptyTiles = 0;
				if (nextAction == PacmanAction.GO_EAST) {
					nextAction = PacmanAction.GO_NORTH;
				}
				else if (nextAction == PacmanAction.GO_SOUTH) {
					nextAction = PacmanAction.GO_EAST;
				}
				else if (nextAction == PacmanAction.GO_WEST) {
					nextAction = PacmanAction.GO_SOUTH;
				}
				else if (nextAction == PacmanAction.GO_NORTH) {
					nextAction = PacmanAction.GO_WEST;
				}
			}
		}


		return nextAction;
	}

	@Override
	protected void onGameStart(PacmanStartInfo startInfo) {

	}

	@Override
	protected void onGameover(PacmanGameResult gameResult) {

	}
}
