package de.fh.stud.p1;

import de.fh.kiServer.agents.Agent;
import de.fh.kiServer.util.Util;
import de.fh.pacman.PacmanAgent_2021;
import de.fh.pacman.PacmanGameResult;
import de.fh.pacman.PacmanPercept;
import de.fh.pacman.PacmanStartInfo;
import de.fh.pacman.enums.PacmanAction;
import de.fh.pacman.enums.PacmanActionEffect;

public class ITA_Agent_BB extends PacmanAgent_2021 {

	private PacmanAction nextAction;

	private boolean firstHit = false;

	public ITA_Agent_BB(String name) {
		super(name);
	}

	public static void main(String[] args) {
		ITA_Agent_BB agent = new ITA_Agent_BB("ITA_Agent_BB");
		Agent.start(agent, "127.0.0.1", 5000);
	}

	/**
	 * @param percept
	 *            - Aktuelle Wahrnehmung des Agenten, bspw. Position der Geister und
	 *            Zustand aller Felder der Welt.
	 * @param actionEffect
	 *            - Aktuelle Rückmeldung des Server auf die letzte übermittelte
	 *            Aktion.
	 */
	@Override
	public PacmanAction action(PacmanPercept percept, PacmanActionEffect actionEffect) {

		// Gebe den aktuellen Zustand der Welt auf der Konsole aus
		Util.printView(percept.getView());

		if (actionEffect == PacmanActionEffect.GAME_INITIALIZED) {
			nextAction = PacmanAction.GO_EAST;
		} else if (actionEffect == PacmanActionEffect.BUMPED_INTO_WALL) {
			if (!firstHit) {
				firstHit = true;
				nextAction = PacmanAction.GO_WEST;
			} else {
				rotate();
			}
		}

		return nextAction;
	}

	private void rotate() {
		switch (nextAction) {
		case GO_EAST:
			nextAction = PacmanAction.GO_SOUTH;
			break;
		case GO_SOUTH:
			nextAction = PacmanAction.GO_WEST;
			break;
		case GO_WEST:
			nextAction = PacmanAction.GO_NORTH;
			break;
		default:
			nextAction = PacmanAction.GO_EAST;
			break;
		}
	}

	@Override
	protected void onGameStart(PacmanStartInfo startInfo) {

	}

	@Override
	protected void onGameover(PacmanGameResult gameResult) {

	}
}
